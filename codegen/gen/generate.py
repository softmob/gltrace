import os
import sys
import getopt
import xml.etree.ElementTree as ET
from jinja2 import Template

class GenStream:
    def __init__(self, api):
        self.types = None
        self.enums = None
        self.commands = None
        self.api = api
        self.templates_path = os.path.join(getPath(), "templates")

    def generateTypes(self, out_dir):
        file_types_in = open(os.path.join(self.templates_path, "types.h.in"))
        src = Template(file_types_in.read())
        file_types_in.close()

        list_types = []
        for t in self.types:
            for u in t.raw.split("\n"):
                list_types.append(u)

        #if self.api == "wgl":
        #   list_types = []
        result = src.render(current_namespace = self.api, list_types = list_types)

        if not os.path.exists(out_dir):
            os.mkdir(out_dir)
        fout = open(os.path.join(out_dir, "types.h"), 'w')
        fout.write(result)
        fout.close()

    def generateEnums(self, out_dir):
        file_enums_in = open(os.path.join(self.templates_path, "enums.h.in"))
        src = Template(file_enums_in.read())
        file_enums_in.close()

        enum_type = None
        for type in self.types:
            if type.name == "GLenum":
                enum_type = type.typename

        for enum in self.enums:
            if enum.value[0] == '-':
                enum.value = "static_cast<" + enum_type + ">(" + enum.value + ")"
        result = src.render(current_namespace = self.api, list_enums = self.enums, enum_type = enum_type)

        if not os.path.exists(out_dir):
            os.mkdir(out_dir)
        fout = open(os.path.join(out_dir, "enums.h"), 'w')
        fout.write(result)
        fout.close()

    def generateCommandsForFile(self, out_dir, filename):
        file_commands_in = open(os.path.join(self.templates_path, filename + ".in"))
        src = Template(file_commands_in.read())
        file_commands_in.close()

        result = src.render(current_namespace = self.api, commands = self.commands)

        if not os.path.exists(out_dir):
            os.mkdir(out_dir)
        fout = open(os.path.join(out_dir, filename), 'w')
        fout.write(result)
        fout.close()

    def generateCommands(self, out_dir):
        self.generateCommandsForFile(out_dir, "commands_type.h")
        self.generateCommandsForFile(out_dir, "commands.cpp")

    def setTypes(self, types):
        self.types = types

    def setEnums(self, enums):
        self.enums = enums

    def setCommands(self, commands):
        self.commands = commands

    def generate(self, out_dir):
        if self.types != None:
            self.generateTypes(out_dir)

        if self.enums != None:
            self.generateEnums(out_dir)

        if self.commands != None:
            self.generateCommands(out_dir)

class Type:
    def __init__(self, xml):
        if "name" in xml.attrib:
            self.name = xml.attrib["name"]
        else:
            self.name = str(xml.find("name").text)

        self.raw = str("".join([ t for t in xml.itertext() ]))
        if "typedef" in self.raw:
            self.raw = self.raw.replace(" " + self.name, " ")
            self.raw = self.raw.replace("*" + self.name, "*")

        if xml.find("apientry") != None:
            self.raw = self.raw.replace("( *)", "(APIENTRY *)")

        if self.name.startswith("struct "):
            self.raw = "struct " + self.name[7:] + ';'

        self.raw = self.raw.replace(" ;", ';')
        self.raw = self.raw.replace(",", ', ')

        self.typename = self.raw.replace("typedef ", "").replace(";", "")

        self.raw = self.raw.replace("typedef ", "using %s = " % self.name)

def parseTypes(api_xml, api_type):
    types = []
    for T in api_xml.iter("types"):
        for type in T.findall("type"):
            if "api" in type.attrib and type.attrib["api"] != api_type:
                continue

            if "name" in type.attrib and type.attrib["name"] not in ["GLhandleARB"]:
                continue
            types.append(Type(type))
    return types

class Enum:
    def __init__(self, xml):
        self.name = xml.attrib["name"]
        self.value = xml.attrib["value"]

def parseEnums(api_xml, api_type):
    enums = []
    for E in api_xml.iter("enums"):
        for enum in E.findall("enum"):
            if "api" in enum.attrib and enum.attrib["api"] != api_type:
                continue
            if "type" in enum.attrib and enum.attrib["type"] == "ull":
                continue
            enums.append(Enum(enum))
    return enums

class Commands:
    def __init__(self, xml):
        self.name = str(xml.find("proto").find("name").text)
        self.ret_type = str("".join([ t for t in xml.find("proto").itertext() if t != self.name ])).strip()

        self.param = []
        for param in xml.findall("param"):
            name = str(param.find("name").text)

            ptype = ""
            if param.text != None:
                ptype = str(param.text)
            if param.find("ptype") != None:
                ptype += str(param.find("ptype").text)
                ptype += str(param.find("ptype").tail)

            ptype_tail = ""
            if param.find("name") != None and param.find("name").tail != None:
                ptype_tail = str(param.find("name").tail)
            if '[' in ptype_tail:
                ptype_tail = ""
                ptype += "*"

            ptype = ptype.replace("  ", " ").strip()
            self.param.append({ "name" : name, "ptype" : ptype, "ptype_tail" : ptype_tail })

def parseCommands(api_xml, api_type):
    commands = []
    for C in api_xml.iter("commands"):
        for command in C.findall("command"):
            commands.append(Commands(command))
    return commands

def parseApi(api_path, api_type):
    tree = ET.parse(api_path)
    api_xml = tree.getroot()

    gen_stream = GenStream(api_type)
    gen_stream.setTypes(parseTypes(api_xml, api_type))
    gen_stream.setEnums(parseEnums(api_xml, api_type))
    gen_stream.setCommands(parseCommands(api_xml, api_type))

    return gen_stream

def streamApi(gen_stream, out_dir):
    gen_stream.generate(out_dir)

def getPath():
    return os.path.dirname(os.path.realpath(__file__))

def usage():
    print("usage: %s -s <GL spec>" % sys.argv[0])

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "s:o:", ["spec=", "outdir="])
    except getopt.GetoptError:
        usage()
        sys.exit(1)

    api_xml_path = None
    out_dir_path = None
    for opt, arg in opts:
        if opt in ("-s", "--spec"):
            api_xml_path = arg
        if opt in ("-o", "--outdir"):
            out_dir_path = arg

    if api_xml_path == None:
        usage()
        sys.exit(1)
    api_xml_path = os.path.join(getPath(), api_xml_path)

    api = "api"
    if "wgl.xml" in api_xml_path:
        api = "wgl"
    elif "gl.xml" in api_xml_path:
        api = "gl"

    if out_dir_path == None:
        out_dir_path = os.path.join(getPath(), "res")

    if not os.path.exists(out_dir_path):
        os.makedirs(out_dir_path)

    streamApi(parseApi(api_xml_path, api), os.path.join(out_dir_path, api))

if __name__ == "__main__":
    main()