set(GL_OUTPUT_FILES
    "${CMAKE_BINARY_DIR}/gen/gl/commands.cpp"
    "${CMAKE_BINARY_DIR}/gen/gl/commands_type.h"
    "${CMAKE_BINARY_DIR}/gen/gl/enums.h"
    "${CMAKE_BINARY_DIR}/gen/gl/types.h"
)

add_custom_command(
    OUTPUT ${GL_OUTPUT_FILES}
    DEPENDS ${project_root}/codegen/gen/gl.xml ${project_root}/codegen/gen/generate.py 
    COMMAND python ${project_root}/codegen/gen/generate.py -s gl.xml -o "${CMAKE_BINARY_DIR}/gen"
    WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
)

add_library(hooks_gl OBJECT
    ${GL_OUTPUT_FILES}
)

target_include_directories(hooks_gl
    PUBLIC
        ${CMAKE_BINARY_DIR}/gen
        ${project_root}/common
)
