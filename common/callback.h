#pragma once

#include <stdint.h>
#include <map>
#include <vector>
#include <string>
#include <export.h>
#include <singleton.h>

enum class type_callback
{
    pre,
    post
};

template<typename Fn, typename ... TupleArgs, std::size_t... index, typename ... Args>
static decltype(auto) call_func_impl(Fn&& fn, std::tuple<TupleArgs...>& tuple, std::index_sequence<index...>, Args&& ... args)
{
    return fn(std::forward<TupleArgs>(std::get<index>(tuple))..., std::forward<Args>(args)...);
}

template<typename Fn, typename ... TupleArgs, typename ... Args>
static decltype(auto) call_func(Fn&& fn, std::tuple<TupleArgs...>& tuple, Args&& ... args)
{
    return call_func_impl(std::forward<Fn>(fn), tuple, std::index_sequence_for<TupleArgs...>{}, std::forward<Args>(args)...);
}

class Callback : public Singleton<Callback>
{
public:
    bool isCallback(const std::string &str, type_callback type)
    {
        auto it_func = _map.find(str);
        if (it_func == _map.end())
            return false;
        auto it_func_call = it_func->second.find(type);
        if (it_func_call == it_func->second.end())
            return false;
        return !it_func_call->second.empty();
    }

    template<typename Fn>
    void addCallbackPre(const std::string& str, Fn func)
    {
        _map[str][type_callback::pre].push_back((void*)func);
    }

    template<typename Fn>
    void addCallbackPost(const std::string& str, Fn func)
    {
        _map[str][type_callback::post].push_back((void*)func);
    }
    
    template<type_callback type, typename Fn, typename ... TupleArgs, typename ... Args>
    void callFunc(const std::string& str, std::tuple<TupleArgs...>& tuple, Args&& ... args)
    {
        auto vec = _map[str][type];
        for (auto it = vec.begin(); it != vec.end(); ++it)
        {
            Fn fn = (Fn)*it;
            call_func(fn, tuple, std::forward<Args>(args)...);
        }
    }
    
private:
    std::map<std::string, std::map<type_callback, std::vector<void*> > > _map;
};