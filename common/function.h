#pragma once

#include <tuple>
#include <string>
#include <functional>
#include <export.h>
#include <callback.h>

template<typename Ret, typename ... Args>
class Function
{
public:
    Function(const std::string & func_name, Args & ... args)
        : func_name(func_name)
        , value_args(args...)
        , func_orig((func_type)HooksMap::Instance().getOrig(func_name))
    {
    }
    
    using Pre = void(*)(Args& ...);
    using Post = void(*)(const Args& ..., Ret&);

    Ret operator() ()
    {
        auto & _processing = Callback::Instance();

        if (_processing.isCallback(func_name, type_callback::pre))
            _processing.callFunc<type_callback::pre, Pre>(func_name, value_args);

        Ret _ret  = call_func(func_orig, value_args);

        if (_processing.isCallback(func_name, type_callback::post))
            _processing.callFunc<type_callback::post, Post>(func_name, value_args, _ret);

        return _ret;
    }

    using func_ret = Ret;
    using func_args = std::tuple<Args&...>;
    using func_type = Ret(APIENTRY *) (Args...);

    func_args value_args;
    func_type func_orig;

    std::string func_name;
};

template<typename ... Args>
class Function<void, Args ...>
{
public:
    Function(const std::string & func_name, Args & ... args)
        : func_name(func_name)
        , value_args(args...)
        , func_orig((func_type)HooksMap::Instance().getOrig(func_name))
    {
    }

    using Pre = void(*)(Args& ...);
    using Post = void(*)(const Args& ...);

    void operator() ()
    {
        auto & _processing = Callback::Instance();

        if (_processing.isCallback(func_name, type_callback::pre))
            _processing.callFunc<type_callback::pre, Pre>(func_name, value_args);

        call_func(func_orig, value_args);

        if (_processing.isCallback(func_name, type_callback::post))
            _processing.callFunc<type_callback::post, Post>(func_name, value_args);
    }

    using func_ret = void;
    using func_args = std::tuple<Args&...>;
    using func_type = func_ret (APIENTRY *) (Args...);

    func_args value_args;
    func_type func_orig;

    std::string func_name;
};
