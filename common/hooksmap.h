#pragma once

#include <stdint.h>
#include <map>
#include <string>
#include <singleton.h>
#include <export.h>

class HooksMap : public Singleton<HooksMap>
{
public:
    void* getOrig(const std::string & str)
    {
        if (!orig.count(str))
            return nullptr;
        return orig[str];
    }
    
    void* getHook(const std::string& str)
    {
        if (!hook.count(str))
            return nullptr;
        return hook[str];
    }

    bool isHook(const std::string& str)
    {
        return hook.find(str) != hook.end();
    }

    bool isOrig(const std::string& str)
    {
        return orig.find(str) != orig.end();
    }
    
    void setOrig(const std::string& str, void* ptr)
    {
        orig[str] = ptr;
    }
    
    void setHook(const std::string& str, void* ptr)
    {
        hook[str] = ptr;
    }
private:
    std::map<std::string, void*> orig;
    std::map<std::string, void*> hook;
};