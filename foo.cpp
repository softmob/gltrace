#include <iostream>

void foo_pre(int x)
{
    std::cout << "foo_pre" << std::endl;
}

void foo_post(int x)
{
    std::cout << "foo_post" << std::endl;
}

struct has_foo_pre
{
    using yes = uint8_t;
    using no = uint16_t;
    template<int _x = 0> static yes impl(decltype(foo_pre));
    static no impl(...);
    static const bool value = sizeof(impl(nullptr)) == sizeof(yes);

    template<bool, int _x = 0>
    struct apply_impl
    {
        static void call(int x)
        {
        }
    };

    template<int _x>
    struct apply_impl<true, _x>
    {
        static void call(int x)
        {
            return foo_pre(x);
        }
    };

    static void apply(int x)
    {
        return apply_impl<value>::call(x);
    }
};

struct has_foo_post
{
    using yes = uint8_t;
    using no = uint16_t;
    template<int _x = 0> static yes impl(decltype(foo_post));
    static no impl(...);
    static const bool value = sizeof(impl(nullptr)) == sizeof(yes);

    template<bool, int _x = 0>
    struct apply_impl
    {
        static void call(int x)
        {
        }
    };

    template<int _x>
    struct apply_impl<true, _x>
    {
        static void call(int x)
        {
            return foo_post(x);
        }
    };

    static void apply(int x)
    {
        return apply_impl<value>::call(x);
    }
};

void foo(int x)
{
    has_foo_pre::apply(x);
    has_foo_post::apply(x);
}

int main(void)
{
    foo(42);
    return 0;
}
