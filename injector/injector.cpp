#include <iostream>
#include <string>
#include <vector>
#include <functional>
#include <deque>
#include <Windows.h>

std::string GetDirectory(const std::string& str)
{
    return str.substr(0, str.find_last_of("\\/"));
}

std::string MakeCommandLine(const std::string& app_path, const std::vector<std::string>& app_args)
{
    std::string command_line = app_path;
    for (auto & arg : app_args)
    {
        command_line += " " + arg;
    }
    return command_line;
}

class ScopeGuard
{
public:
    ~ScopeGuard()
    {
        for (auto & handler : m_deque)
        {
            handler();
        }
    }

    using HandlerType = std::function<void()>;
    ScopeGuard& operator += (const HandlerType& fn)
    {
        m_deque.push_front(fn);
        return *this;
    }

private:
    std::deque<HandlerType> m_deque;
};

/*
*dll_inject made on the basis:
* - https://github.com/apitrace/apitrace/blob/master/inject/injector.cpp (ctrl-c + ctrl-v)
* - http://securityxploded.com/dll-injection-and-hooking.php
*/

bool inject_dll(HANDLE hProcess, const std::string& dll_path)
{
    ScopeGuard guard;

    // Allocate memory in the target process to hold the DLL name
    void* lpMemory = VirtualAllocEx(hProcess, nullptr, dll_path.size() + 1, MEM_COMMIT, PAGE_READWRITE);
    if (!lpMemory)
    {
        std::cout << "failed to allocate memory in the process" << std::endl;
        return false;
    }

    guard += [&]
    {
        VirtualFreeEx(hProcess, lpMemory, 0, MEM_RELEASE);
    };

    // Copy DLL name into the target process
    if (!WriteProcessMemory(hProcess, lpMemory, dll_path.c_str(), dll_path.size() + 1, nullptr))
    {
        std::cout << "failed to write into process memory" << std::endl;
        return false;
    }

    /*
    * Get LoadLibraryA address from kernel32.dll.  It's the same for all the
    * process (XXX: but only within the same architecture).
    */
    PTHREAD_START_ROUTINE lpStartAddress = (PTHREAD_START_ROUTINE)GetProcAddress(GetModuleHandleA("kernel32.dll"), "LoadLibraryA");

    // Create remote thread in another process
    HANDLE hThread = CreateRemoteThread(hProcess, nullptr, 0, lpStartAddress, lpMemory, 0, nullptr);
    if (!hThread)
    {
        std::cout << "failed to create remote thread" << std::endl;
        return false;
    }

    guard += [&]
    {
        CloseHandle(hThread);
    };

    // Wait for it to finish
    WaitForSingleObject(hThread, INFINITE);
    DWORD hModuleExitCode = 0;
    GetExitCodeThread(hThread, &hModuleExitCode);
    if (!hModuleExitCode)
    {
        std::cout << "inject: error: failed to load "
            << dll_path << " into the remote process" << std::endl;
        return false;
    }

    return true;
}

bool launch_app(const std::string app_path, std::vector<std::string>& app_args, const std::string& dll_path)
{
    ScopeGuard guard;

    std::string command_line = MakeCommandLine(app_path, app_args);
    std::string work_directory = GetDirectory(app_path);

    STARTUPINFOA startup_info = {};
    PROCESS_INFORMATION process_info = {};

    BOOL is_run = CreateProcessA(
        nullptr,                                  // lpApplicationName,
        const_cast<char *>(command_line.c_str()), // lpCommandLine,
        nullptr,                                  // lpProcessAttributes,
        nullptr,                                  // lpThreadAttributes,
        FALSE,                                    // bInheritHandles,
        CREATE_SUSPENDED,                         // dwCreationFlags,
        nullptr,                                  // lpEnvironment,
        work_directory.c_str(),                   // lpCurrentDirectory,
        &startup_info,                            // lpStartupInfo,
        &process_info                             // lpProcessInformation
    );

    if (!is_run)
    {
        std::cout << "error: failed to execute " << command_line
            << " with GetLastError: " << GetLastError() << std::endl;
        return false;
    }

    guard += [&]
    {
        CloseHandle(process_info.hThread);
        CloseHandle(process_info.hProcess);
    };

    if (!inject_dll(process_info.hProcess, dll_path))
    {
        TerminateProcess(process_info.hProcess, ~0);
        return false;
    }

    ResumeThread(process_info.hThread);
    WaitForSingleObject(process_info.hProcess, INFINITE);
    return true;
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        fprintf(stderr,
            "usage:\n"
            "injector <command> [args] ...\n");
        return ~0;
    }

    std::string app_path = argv[1];
    std::vector<std::string> app_args;
    for (int i = 2; i < argc; ++i)
    {
        app_args.push_back(argv[i]);
    }

    char szDllPath[MAX_PATH];
    GetModuleFileNameA(nullptr, szDllPath, sizeof szDllPath);
    std::string dll_path(szDllPath);
    dll_path = GetDirectory(dll_path) + "\\" + "interceptor.dll";

    return launch_app(app_path, app_args, dll_path);
}
