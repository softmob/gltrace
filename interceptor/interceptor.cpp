#include <cstdio>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <cassert>
#include <Windows.h>
#include <tlhelp32.h>
#include <memory>
#include <hooksmap.h>
#include <callback.h>
#include <wgl/commands_type.h>
#include <gl/commands_type.h>

struct ModuleState
{
    using FunctionMap = std::map<std::string, LPVOID>;
    FunctionMap functionMap;

    HMODULE hThisModule = nullptr;
    HooksMap* hooksTable = nullptr;

    std::set<std::string> moduleForHooks;

    std::set<HMODULE> interceptedModule;
} moduleState;

std::string getFileName(const char *path)
{
    std::string file(path);
    file = file.substr(file.find_last_of('\\') + 1);
    for (auto &ch : file)
        ch = ::tolower(ch);
    return file;
}

BOOL replaceAddress(LPVOID *lpOldAddress, LPVOID lpNewAddress)
{
    DWORD flOldProtect;

    if (*lpOldAddress == lpNewAddress)
        return TRUE;

    if (!(VirtualProtect(lpOldAddress, sizeof *lpOldAddress, PAGE_READWRITE, &flOldProtect)))
        return FALSE;

    *lpOldAddress = lpNewAddress;

    if (!(VirtualProtect(lpOldAddress, sizeof *lpOldAddress, flOldProtect, &flOldProtect)))
        return FALSE;

    return TRUE;
}

void registerModuleHooks(const std::string &moduleName, HMODULE hReplaceModule)
{
    assert(((PIMAGE_DOS_HEADER)hReplaceModule)->e_magic == IMAGE_DOS_SIGNATURE);
    PIMAGE_NT_HEADERS header = (PIMAGE_NT_HEADERS)((PBYTE)hReplaceModule + ((PIMAGE_DOS_HEADER)hReplaceModule)->e_lfanew);
    assert(header->Signature == IMAGE_NT_SIGNATURE);
    assert(header->OptionalHeader.NumberOfRvaAndSizes > 0);

    PIMAGE_EXPORT_DIRECTORY pExportDescriptor = (PIMAGE_EXPORT_DIRECTORY)((PBYTE)hReplaceModule + header->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress);

    PDWORD pAddressOfNames = (PDWORD)((PBYTE)hReplaceModule + pExportDescriptor->AddressOfNames);
    PDWORD pAddressOfFunctions = (PDWORD)((PBYTE)hReplaceModule + pExportDescriptor->AddressOfFunctions);

    for (DWORD i = 0; i < pExportDescriptor->NumberOfNames; ++i)
    {
        const char *szFunctionName = (const char *)((PBYTE)hReplaceModule + pAddressOfNames[i]);
        LPVOID lpNewAddress = (LPVOID)((PBYTE)hReplaceModule + pAddressOfFunctions[i]);
        assert(lpNewAddress == (LPVOID)GetProcAddress(hReplaceModule, szFunctionName));

        std::string fooName(szFunctionName);
        while (fooName.front() == '_')
            fooName.erase(0, 1);
        std::string pref = "Hook_";
        if (fooName.find(pref) == 0)
            fooName = fooName.substr(pref.size());
        size_t loc = fooName.find('@');
        fooName = fooName.substr(0, loc);

        moduleState.functionMap[fooName.c_str()] = lpNewAddress;
        moduleState.hooksTable->setHook(fooName, lpNewAddress);
        //printf("Export: %s %p\n", fooName.c_str(), lpNewAddress);
    }
}

void setHooksOnModule(HMODULE hModule, const char* szModule)
{
    if (hModule == moduleState.hThisModule)
        return;

    if (!moduleState.interceptedModule.insert(hModule).second)
        return;

    std::string szBaseName = getFileName(szModule);

    static std::string bannedList[] = { "ntdll.dll", "kernel32.dll" };
    if (std::count(std::cbegin(bannedList), std::cend(bannedList), szBaseName))
        return;

    assert(((PIMAGE_DOS_HEADER)hModule)->e_magic == IMAGE_DOS_SIGNATURE);
    PIMAGE_NT_HEADERS header = (PIMAGE_NT_HEADERS)((PBYTE)hModule + ((PIMAGE_DOS_HEADER)hModule)->e_lfanew);
    assert(header->Signature == IMAGE_NT_SIGNATURE);
    assert(header->OptionalHeader.NumberOfRvaAndSizes > 0);
    PIMAGE_IMPORT_DESCRIPTOR pImgImportDesc = (PIMAGE_IMPORT_DESCRIPTOR)((PBYTE)hModule + header->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);

    for (; pImgImportDesc && pImgImportDesc->FirstThunk; ++pImgImportDesc)
    {
        const char *szDescriptorName = (const char *)((PBYTE)hModule + pImgImportDesc->Name);

        if (!moduleState.moduleForHooks.count(getFileName(szDescriptorName)))
            continue;

        PIMAGE_THUNK_DATA pThunkIAT = (PIMAGE_THUNK_DATA)((PBYTE)hModule + pImgImportDesc->FirstThunk);
        PIMAGE_THUNK_DATA pThunk = pThunkIAT;
        if (pImgImportDesc->OriginalFirstThunk)
            pThunk = pThunk = (PIMAGE_THUNK_DATA)((PBYTE)hModule + pImgImportDesc->OriginalFirstThunk);

        while (pThunk->u1.Function)
        {
            if (!(pImgImportDesc->OriginalFirstThunk == 0 || pThunk->u1.Ordinal & IMAGE_ORDINAL_FLAG))
            {
                PIMAGE_IMPORT_BY_NAME pImport = (PIMAGE_IMPORT_BY_NAME)((PBYTE)hModule + pThunk->u1.AddressOfData);
                const char* szName = (const char*)pImport->Name;
                LPVOID * addr_offset = (LPVOID *)(&pThunkIAT->u1.Function);
                LPVOID orig_addr = *addr_offset;

                std::string funcName(szName);
                if (moduleState.functionMap.count(funcName))
                {
                    LPVOID new_addr = moduleState.functionMap[funcName];
                    if (replaceAddress(addr_offset, new_addr))
                    {
                        if (!moduleState.hooksTable->getOrig(funcName))
                            moduleState.hooksTable->setOrig(funcName, orig_addr);

                        printf("sethooks on %s\n", funcName.c_str());
                    }
                }
            }

            ++pThunk;
            ++pThunkIAT;
        }
    }
}

#include <codecvt>

void patchAllModules()
{
    HANDLE hModuleSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, GetCurrentProcessId());
    if (hModuleSnap == INVALID_HANDLE_VALUE)
        return;

    MODULEENTRY32 me32;
    me32.dwSize = sizeof me32;

    std::wstring_convert<std::codecvt_utf8<wchar_t>> convert;

    if (Module32First(hModuleSnap, &me32))
    {
        do
        {
            setHooksOnModule(me32.hModule, convert.to_bytes(me32.szExePath).c_str());
        } while (Module32Next(hModuleSnap, &me32));
    }

    CloseHandle(hModuleSnap);
}

extern "C" DLLEXPORT
FARPROC WINAPI Hook_GetProcAddress(HMODULE hModule, LPCSTR lpProcName)
{
    char szModule[MAX_PATH];
    DWORD dwRet = GetModuleFileNameA(hModule, szModule, sizeof szModule);
    assert(dwRet);

    std::string szBaseName = getFileName(szModule);
    if (moduleState.moduleForHooks.count(szBaseName))
    {
        std::string funcName(lpProcName);

        if (moduleState.functionMap.count(funcName))
        {
            printf("GetProcAddress %s\n", funcName.c_str());

            if (!moduleState.hooksTable->getOrig(funcName))
                moduleState.hooksTable->setOrig(funcName, GetProcAddress(hModule, lpProcName));
            return (FARPROC)moduleState.functionMap[funcName];
        }
    }
    return GetProcAddress(hModule, lpProcName);
}

extern "C" DLLEXPORT
HMODULE WINAPI Hook_LoadLibraryA(LPCSTR lpLibFileName)
{
    patchAllModules();
    return LoadLibraryA(lpLibFileName);
}

extern "C" DLLEXPORT
HMODULE WINAPI Hook_LoadLibraryW(LPCWSTR lpLibFileName)
{
    patchAllModules();
    return LoadLibraryW(lpLibFileName);
}

extern "C" DLLEXPORT
HMODULE WINAPI Hook_LoadLibraryExA(LPCSTR lpLibFileName, HANDLE hFile, DWORD dwFlags)
{
    patchAllModules();
    return LoadLibraryExA(lpLibFileName, hFile, dwFlags);
}

extern "C" DLLEXPORT
HMODULE WINAPI Hook_LoadLibraryExW(LPCWSTR lpLibFileName, HANDLE hFile, DWORD dwFlags)
{
    patchAllModules();
    return LoadLibraryExW(lpLibFileName, hFile, dwFlags);
}

void c_wglGetProcAddress(const LPCSTR& lpszProc, PROC& ret)
{
    if (moduleState.hooksTable->isHook(lpszProc) && moduleState.hooksTable->isOrig(lpszProc))
        ret = (wgl::wglGetProcAddress_type::func_ret)moduleState.hooksTable->getHook(lpszProc);
}

void c_glClearColor(GLclampf& red, GLclampf& green, GLclampf& blue, GLclampf& alpha)
{
    red = 0.0f;
    green = 0.2f;
    blue = 0.4f;
    alpha = 1.0f;
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved)
{
    switch (fdwReason)
    {
    case DLL_PROCESS_ATTACH:
    {
        moduleState.hThisModule = hinstDLL;
        moduleState.hooksTable = &HooksMap::Instance();

        registerModuleHooks("interceptor.dll", moduleState.hThisModule);

        moduleState.moduleForHooks.insert("opengl32.dll");
        moduleState.moduleForHooks.insert("kernel32.dll");
        moduleState.moduleForHooks.insert("gdi32.dll");

        /*
        * Hook kernel32.dll functions, and its respective Windows API Set.
        *
        * http://msdn.microsoft.com/en-us/library/dn505783.aspx (Windows 8.1)
        * http://msdn.microsoft.com/en-us/library/hh802935.aspx (Windows 8)
        */

        //Windows 8 API Sets
        moduleState.moduleForHooks.insert("api-ms-win-core-libraryloader-l1-1-1.dll");
        //Windows 8.1 API Sets
        moduleState.moduleForHooks.insert("api-ms-win-core-libraryloader-l1-2-0.dll");

        patchAllModules();

        printf("interceptor\n");
        auto & _processing = Callback::Instance();
        _processing.addCallbackPost<wgl::wglGetProcAddress_type::Post>("wglGetProcAddress", c_wglGetProcAddress);
        _processing.addCallbackPre<gl::glClearColor_type::Pre>("glClearColor", c_glClearColor);
        break;
    }
    case DLL_THREAD_ATTACH:
        break;
    case DLL_THREAD_DETACH:
        break;
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}